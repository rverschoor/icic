# frozen_string_literal: true

require_relative 'graphql'

# Training class
class Pairing < Graphql

  attr_reader :dates

  QUERY = <<~GRAPHQL.freeze
    query ($cursor: String!, $first: Int!) {
      project(fullPath: \"gitlab-com/support/support-pairing\") {
        issues(first: $first, after: $cursor) {
          pageInfo { endCursor hasNextPage }
          nodes {
            createdAt
            assignees {
              nodes {
                username
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def initialize
    super
    @dates = {}
    @delay = 1
  end

  def process
    collect(QUERY)
  end

  private

  def page_info
    json['data']['project']['issues']['pageInfo']
  end

  def nodes
    json['data']['project']['issues']['nodes']
  end

  def count
    nodes.each do |node|
      d = Date.parse(node['createdAt'])
      date = d.strftime('%Y-%m')
      @dates[date] = (@dates[date] || 0) + node['assignees']['nodes'].length
    end
  end
end
