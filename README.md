# IC Issue Count

## Training issues
Count # of issues with label `module` in `gitlab-com/support/support-training`.

## Pairing issues
Count # of assignees on issues in `gitlab-com/support/support-pairing`.

Expects environment variable `GL_TOKEN` with a GitLab personal access token.