#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require_relative 'pairing'
require_relative 'training'

# Main class
class Icic
  def training
    puts '=== Training ==='
    data = Training.new
    data.process
    report data.dates
  end

  def pairing
    puts '=== Pairing ==='
    data = Pairing.new
    data.process
    report data.dates
  end

  def report(dates)
    dates.sort
    dates.each do |k, v|
      puts "#{k} #{v}"
    end
    puts
  end
end

icic = Icic.new
icic.training
icic.pairing
