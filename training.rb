# frozen_string_literal: true

require_relative 'graphql'

# Training class
class Training < Graphql

  attr_reader :dates

  QUERY = <<~GRAPHQL
    query ($cursor: String!, $first: Int!) {
      project(fullPath: \"gitlab-com/support/support-training\") {
        issues(labelName: \"module\", first: $first, after: $cursor) {
          pageInfo { endCursor hasNextPage }
          nodes {
            createdAt
          }
        }
      }
    }
  GRAPHQL

  def initialize
    super
    @dates = {}
  end

  def process
    collect(QUERY)
  end

  private

  def page_info
    json['data']['project']['issues']['pageInfo']
  end

  def nodes
    json['data']['project']['issues']['nodes']
  end

  def count
    nodes.each do |issue|
      d = Date.parse(issue['createdAt'])
      date = d.strftime('%Y-%m')
      @dates[date] = (@dates[date] || 0) + 1
    end
  end
end
